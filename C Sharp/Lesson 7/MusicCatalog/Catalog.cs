﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MusicCatalog
{
    class Catalog
    {
        public string Name { get; private set; }
        public List<CD> CDList { get; private set; }
        public Catalog(string name)
        {
            Name = name;
            CDList = new List<CD>();
        }
        public void AddCD(CD cd)
        {
            CDList.Add(cd);
        }
        public void DeleteCD(int index)
        {
            CDList.RemoveAt(index);
        }
        public void FindSong(string authorSong)
        {
            Console.WriteLine("Find result (aut. {0}): ",authorSong);
            List<Song> find = new List<Song>();
            foreach (var catalog in CDList)
            {
                find.AddRange(catalog.SongList.Where(o => o.Author==authorSong));
            }
            if (find.Count == 0)
            {
                Console.WriteLine("   The list is empty!");
                return;
            }
            foreach (var song in find)
            {
                Console.WriteLine(" - {0}", song.Name);
            }
        }
        public void ViewCatalog()
        {
            Console.WriteLine("CD name: {0}", Name);
            foreach (var catalog in CDList)
            {
                ViewCatalog(catalog);
            }
        }
        public void ViewCatalog(CD dir)
        {
            Console.WriteLine("Catalog: {0}", dir.Name);
            if (dir.SongList.Count > 0)
                foreach (var song in dir.SongList)
                    Console.WriteLine("   {0} (aut.: {1})", song.Name, song.Author);
            else
                Console.WriteLine("   <Empty catalog>");
        }
    }
}

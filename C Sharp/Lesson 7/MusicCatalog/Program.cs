﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MusicCatalog
{
    class Program
    {
        static void Main(string[] args)
        {
            Catalog catalog = new Catalog("My music 1");

            CD jazz = new CD("Jazz");
            jazz.AddSong(new Song("Song1", "Bit"));
            jazz.AddSong(new Song("Song2"));
            jazz.AddSong(new Song("Song3"));
            jazz.DeleteSong(1);
            catalog.AddCD(jazz);

            CD test = new CD("test");
            catalog.AddCD(test);
            catalog.DeleteCD(1);

            CD rock = new CD("Rock");
            catalog.AddCD(rock);

            CD myCD = new CD("Live music");
            myCD.AddSong(new Song("SuperSong", "Bit"));
            catalog.AddCD(myCD);

            catalog.ViewCatalog();
            Console.WriteLine();
            catalog.FindSong("Bit");
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MusicCatalog
{
    class CD
    {
        public string Name { get; private set; }
        public List<Song> SongList { get; private set; }
        public CD(string name)
        {
            Name = name;
            SongList = new List<Song>();
        }

        public void AddSong(Song song)
        {
            SongList.Add(song);
        }
        public void DeleteSong(int index)
        {
            SongList.RemoveAt(index);
        }
    }
}

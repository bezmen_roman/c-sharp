﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MusicCatalog
{
    class Song
    {
        public string Name { get; private set; }
        public string Author { get; private set; }
        public Song(string name, string author = "unknown")
        {
            Name = name;
            Author = author;
        }
    }
}

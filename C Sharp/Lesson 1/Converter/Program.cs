﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Converter
{
    enum Сurrency
    {
        evro,
        hryvnia,
        ruble
    }

    class Program
    {
        static double CourseOfExchange(int v)
        {
            if (v == (int)Сurrency.evro)
                return 10.3;
            if (v == (int)Сurrency.hryvnia)
                return 8.2;
            if (v == (int)Сurrency.ruble)
                return 40.1;
            return 0;
        }


        static void Main(string[] args)
        {
            Console.Write("Money (in dollars): ");
            string s = Console.ReadLine();
            double money;

            if (!double.TryParse(s, out money))
            {
                Console.WriteLine("Error value!");
                return;
            }

            Console.Write("Valuta (0 - Evro, 1 - Hryvnia, 2 - Ruble): ");
            s = Console.ReadLine();
            int val;

            if (!int.TryParse(s, out val))
            {
                Console.WriteLine("Error value!");
                return;
            }

            if (CourseOfExchange(val) != 0)
            {
                Console.WriteLine("Result: {0} {1}", money * CourseOfExchange(val), (Сurrency)val);
            }
            else
            {
                Console.WriteLine("Error value!");
            }

        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Student
{
    public struct Student
    {
        public string UserName { get; private set; }
        public string LastName { get; private set; }
        public string Group { get; private set; }
        public int Age { get; private set; }
        public Student(string userName, string lastName, string group, int age): this()
        {
            UserName = userName;
            LastName = lastName;
            Group = group;
            Age = age;
        }
        public override string ToString()
        {
            return string.Format("First name: {0}\nSecond name: {1}\nGroup: {2}\nAge: {3}",UserName,LastName,Group,Age);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StringRevers
{
    class Program
    {
        static void Main(string[] args)
        {
            string str;
            StringBuilder res = new StringBuilder();
            str = Console.ReadLine();
            string[] s = str.Split(' ');
            for (int i = s.Length - 1; i >= 0; i--)
            {
                res.Append(s[i]);
                res.Append(' ');
            }
            Console.WriteLine(res);
        }
    }
}

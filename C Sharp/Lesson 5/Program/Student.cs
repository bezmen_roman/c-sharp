﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Person_Teacher_Student
{
    class Student : Person
    {
        public int Course { get; private set; }
        private static Student[] _students = {
                                             new Student("Ivan Ivanov", 21, 3),
                                             new Student("Petr Petrov", 22, 4),
                                             new Student("Denis Denisov", 18, 1)
                                             };
        public Student(string name, int age, int course):base(name,age){
            Course = course;
        }
        public static Student RandomStudent()
        {
            Random rnd = new Random();
            int r = rnd.Next(_students.Count());
            return _students[r];
        }
        public override void Print()
        {
            Console.WriteLine("Status: student \nName: {0} \nAge: {1}\nCourse: {2}",Name, Age,Course);
        }
        
        public override string ToString()
        {
            return string.Format("Status: student \nName: {0} \nAge: {1}\nCourse: {2}", Name, Age, Course);
        }
        public override object Clone()
        {
            return new Student(Name, Age, Course+1);
        }
    }
}

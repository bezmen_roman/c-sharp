﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Person_Teacher_Student
{
    class Person : ICloneable
    {
        public string Name { get; private set; }
        public int Age { get; private set; }
        private static Person[] _persons = {new Person("Ivan Ivanov",45),
                                            new Person("Petr Petrov",34),
                                            new Person("Denis Denisov",18)
                                           };
        public Person(string name, int age)
        {
            Name = name;
            Age = age;
        }
        public static Person RandomPerson(){
            Random rnd = new Random();
            int r = rnd.Next(_persons.Count());
            return _persons[r];
        }
        public virtual void Print(){
            Console.WriteLine("Name: {0}\nAge: {1}",Name,Age);   
        }
        
        public override string ToString()
        {
            return string.Format("Name: {0}\nAge: {1}", Name, Age);
        }

        public virtual object Clone()
        {
            return new Person(Name,Age);
        }
    }
}

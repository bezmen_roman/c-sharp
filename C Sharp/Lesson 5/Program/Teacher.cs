﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Person_Teacher_Student
{
    class Teacher : Person
    {
        private static Teacher[] _teachers = {
                                              new Teacher("Ivan Ivanov", 40),
                                              new Teacher("Petr Petrov", 30),
                                              new Teacher("Denis Denisov", 50)
                                             };
        public Teacher(string name, int age):base(name,age){}
        public static Teacher RandomTeacher()
        {
            Random rnd = new Random();
            int r = rnd.Next(_teachers.Count());
            return _teachers[r];
        }
        public override void Print()
        {
            Console.WriteLine("Status: teacher \nName: {0} \nAge: {1}", Name, Age);
        }
        
        public override string ToString()
        {
            return string.Format("Status: teacher \nName: {0} \nAge: {1}",Name, Age);
        }
        public override object Clone()
        {
            return new Teacher(Name,Age);
        }
    }
}

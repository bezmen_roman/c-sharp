﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Person_Teacher_Student
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Person> list = new List<Person>();
            list.Add(new Student("Sergey Ivanov", 20, 3));
            list.Add(new Student("Denis Bobrov", 19, 3));
            list.Add(new Student("Oleh Volkov", 21, 4));
            list.Add(new Teacher("Ivan Samohin",46));
            list.Add(new Student("Oleh Volkov", 21, 4));
            list.Add(new Teacher("Nikita Orlov", 30));
            list.Add(new Person("Anna Timohina",78));

            int s = 0, t = 0;
            foreach (var item in list)
            {
                if (item is Student)
                    s++;
                if (item is Teacher)
                    t++;
	            Console.WriteLine(item+"\n");
            }
            Console.WriteLine("Students: {0}\nTeachers: {1}\nPersons: {2}\n",s,t,list.Count-s-t);

            Type type = typeof(Student);
            Console.WriteLine("list[0]\nCurrent type: {0}\nBase type: {1}\n",list[0].GetType(),type.BaseType);

            Console.WriteLine("Clonable person:");
            Person a = (Person)list[0].Clone();
            Console.WriteLine(a+"\n");

            Console.WriteLine("Random person:");
            Console.WriteLine(Person.RandomPerson());
        }
    }
}

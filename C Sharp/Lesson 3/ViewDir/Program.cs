﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ViewDir
{
    class Program
    {
        static void ViewDir(DirectoryInfo dir)
        {
            foreach (var item in dir.GetDirectories())
            {
                Console.WriteLine("[DIR] {0}", item);
                ViewDir(item);
            }
            foreach (var item in dir.GetFiles())
            {
                Console.WriteLine("[FILE] {0}", item);
            }
        }
        static void Main(string[] args)
        {
            DirectoryInfo dir = new DirectoryInfo(@"D:\Методические пособия");
            ViewDir(dir);
        }
    }
}

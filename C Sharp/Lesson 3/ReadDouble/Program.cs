﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace ReadDouble
{
    class Program
    {
        static void Main(string[] args)
        {
            FileStream input = new FileStream(@"C:\Users\student\Desktop\double.txt", FileMode.Open, FileAccess.Read);
            StreamReader read = new StreamReader(input);

            string line;
            double s = 0;
            while ((line = read.ReadLine()) != null)
            {
                string[] nums = line.Split();
                foreach (var item in nums)
                {
                    double n;
                    double.TryParse(item, out n);
                    s += n;
                }
            }

            Console.WriteLine("S = {0}", s);

        }
    }
}
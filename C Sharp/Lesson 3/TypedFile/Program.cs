﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TypedFile
{
    class Program
    {
        static void Main(string[] args)
        {
            FileStream file = new FileStream(@"C:\Users\student\Desktop\int.txt", FileMode.OpenOrCreate, FileAccess.ReadWrite);
            BinaryWriter write = new BinaryWriter(file);
            int[] data = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 0 };
            foreach (var item in data)
            {
                write.Write(item);
            }

            file.Seek(0, SeekOrigin.Begin);
            BinaryReader read = new BinaryReader(file);
            List<int> nums = new List<int>();
            for (int i = 0; i < data.Length; ++i)
            {
                int n = read.ReadInt32();
                nums.Add(n * n);
            }

            file.Seek(0, SeekOrigin.Begin);
            BinaryWriter writeNew = new BinaryWriter(file);
            foreach (var item in nums)
            {
                writeNew.Write(item);
            }

            file.Seek(0, SeekOrigin.Begin);
            BinaryReader print = new BinaryReader(file);
            for (int i = 0; i < nums.Count; ++i)
            {
                int n = print.ReadInt32();
                Console.WriteLine("{0} ", n);
            }

            write.Close();
            read.Close();
            writeNew.Close();
            print.Close();
            file.Close();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Events
{
    class SomeEventArgs: EventArgs
    {
        public string Message { get; set; }
        public SomeEventArgs(string s)
        {
            Message = s;
        }
    }
}

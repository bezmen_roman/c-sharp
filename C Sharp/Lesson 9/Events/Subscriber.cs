﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Events
{
    class Person
    {
        public string Name { get; private set; }
        public Person(string name, Post pub)
        {
            Name = name;
            pub.RaiseSomeEvent += HandleSomeEvent;
        }
        void HandleSomeEvent(object sender, SomeEventArgs e)
        {
            Console.WriteLine("{0} получил сообщение: '{1}'", Name, e.Message);
        }
    }
}

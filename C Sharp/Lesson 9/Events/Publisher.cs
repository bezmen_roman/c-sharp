﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Events
{
    class Post
    {
        public event EventHandler<SomeEventArgs> RaiseSomeEvent;

        public void SendNewspaper()
        {
            OnRaiseSomeEvent(new SomeEventArgs("Получите газету!\n"));
        }
        public void SendMagazine()
        {
            OnRaiseSomeEvent(new SomeEventArgs("Получите журнал!\n"));
        }
        public void SendMail()
        {
            OnRaiseSomeEvent(new SomeEventArgs("Получите почту!\n"));
        }
        protected virtual void OnRaiseSomeEvent(SomeEventArgs e)
        {
            EventHandler<SomeEventArgs> handler = RaiseSomeEvent;
            if (handler != null)
            {
                e.Message += String.Format("Событие произошло в {0}", DateTime.Now);
                handler(this,e);
            }
        }
    }
}

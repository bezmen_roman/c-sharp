﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Events
{
    class Program
    {
        static void Main(string[] args)
        {
            Post pub1 = new Post();
            Post pub2 = new Post();
            Person sub1 = new Person("Иванов", pub1);
            Person sub2 = new Person("Петров", pub2);
            Person sub3 = new Person("Сидоров", pub1);

            pub1.SendNewspaper();
            pub2.SendMail();
        }
    }
}

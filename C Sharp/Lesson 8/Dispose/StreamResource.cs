﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dispose
{
    class StreamResource : IDisposable
    {
        private Stream _fs;
        private bool _disposed;
        public StreamResource(Stream fs)
        {
            _fs = fs;
            _disposed = false;
        }
        public void FileSize()
        {
            Console.WriteLine("Размер файла: {0}", _fs.Length);
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        protected virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    if (_fs != null)
                        _fs.Dispose();
                    Console.WriteLine("Ресурсы освобождены.");
                }
                _fs = null;
                _disposed = true;
            }
        }
    }
}

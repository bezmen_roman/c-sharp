﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dispose
{
    class Program
    {
        static void Main(string[] args)
        {
                FileStream fs = File.OpenRead(@"D:\Log.txt");
                using (StreamResource stream = new StreamResource(fs))
                {
                    stream.FileSize();
                    stream.Dispose();
                }
        }
    }
}

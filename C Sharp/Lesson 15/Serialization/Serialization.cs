﻿using Serialization;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Runtime.Serialization.Formatters.Soap;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;

namespace Serialization
{
    static class Serialization
    {
        static public void XmlSerialize(string dir, object obj)
        {
            FileStream fs = new FileStream(dir, FileMode.Create);
            XmlSerializer xs = new XmlSerializer(obj.GetType());
            xs.Serialize(fs, obj);
            fs.Close();
        }
        static public object XmlDeserialize(string dir, Type type)
        {
            FileStream fs = new FileStream(dir, FileMode.Open);
            XmlSerializer xs = new XmlSerializer(type);
            var data = xs.Deserialize(fs);
            fs.Close();
            return data;
        }
        static public void BinarySerialize(string dir, object obj)
        {
            FileStream fs = new FileStream(dir, FileMode.Create);
            BinaryFormatter bf = new BinaryFormatter();
            bf.Serialize(fs, obj);
            fs.Close();
        }
        static public object BinaryDeserialize(string dir)
        {
            FileStream fs = new FileStream(dir, FileMode.Open);
            BinaryFormatter bf = new BinaryFormatter();
            var data = bf.Deserialize(fs);
            fs.Close();
            return data;
        }
        static public void SoapSerialize(string dir, object obj)
        {
            FileStream fs = new FileStream(dir, FileMode.Create);
            SoapFormatter sf = new SoapFormatter();
            sf.Serialize(fs, obj);
            fs.Close();
        }
        static public object SoapDeserialize(string dir)
        {
            FileStream fs = new FileStream(dir, FileMode.Open);
            SoapFormatter sf = new SoapFormatter();
            var data = sf.Deserialize(fs);
            fs.Close();
            return data;
        }
    }
}

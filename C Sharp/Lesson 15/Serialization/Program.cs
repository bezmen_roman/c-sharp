﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Serialization
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Student> s = new List<Student> { new Student("Alex", 20), new Student("Denis", 18) };
            
            Serialization.XmlSerialize("data.xml", s);
            var xmlDeser = (List<Student>)Serialization.XmlDeserialize("data.xml", typeof(List<Student>));
            foreach (var item in xmlDeser)
            {
                Console.WriteLine("{0} ({1})", item.Name, item.Age);
            }
            Console.WriteLine();

            Serialization.BinarySerialize("data.bin", s);
            var binaryDeser = (List<Student>)Serialization.BinaryDeserialize("data.bin");
            foreach (var item in binaryDeser)
            {
                Console.WriteLine("{0} ({1})", item.Name, item.Age);
            }
            Console.WriteLine();

            Serialization.SoapSerialize("data.soap", s.ToArray());
            var soapDeser = Serialization.SoapDeserialize("data.soap");
            foreach (var item in (Student[])soapDeser)
            {
                Console.WriteLine("{0} ({1})", item.Name, item.Age);
            }
        }
    }
}

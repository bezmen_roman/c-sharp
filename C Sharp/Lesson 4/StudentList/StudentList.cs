﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StudentList
{
    class StudentList
    {
        private List<Student> _list;
        static private int _mark;

        public StudentList()
        {
            _list = new List<Student>();
        }

        public void Add(Student s)
        {
            _list.Add(s);
        }

        public void Sort()
        {
            _list.Sort();
        }

        public List<Student> FindAll(int mark)
        {
            _mark = mark;
            return _list.FindAll(FindStudent);
        }

        public static bool FindStudent(Student s)
        {
            return s.School == _mark;
        }

    }
}

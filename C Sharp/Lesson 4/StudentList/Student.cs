﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StudentList
{
    class Student : IComparable<Student>
    {
        public string Name { get; private set; }
        public int Year { get; private set; }
        public string Address { get; private set; }
        public int School { get; private set; }

        public Student(string name, int year, string address, int school)
        {
            Name = name;
            Year = year;
            Address = address;
            School = school;
        }

        public override string ToString()
        {
            string info;
            info = string.Format("Name: {0} ({1})\nAddress: {2}", Name, Year, Address);
            return info;
        }

        public int CompareTo(Student other)
        {
            return Year - other.Year;
        }
    }
}

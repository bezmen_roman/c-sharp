﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StudentList
{
    class Program
    {
        static void Main(string[] args)
        {
             StudentList students = new StudentList();
            students.Add(new Student("Vasya Pupkin",1980,"Moskow",5));
            students.Add(new Student("Denis Ivanov", 1970, "Minsk", 15));
            students.Add(new Student("Ivan Semenov", 1975, "Moskow", 5));
            students.Add(new Student("Artem Vorona", 1990, "Kremenchug", 3));
            students.Add(new Student("Petr Ivanov", 1978, "Minsk", 5));

            Console.WriteLine("Enter school to find: ");
            int mark;
            string s = Console.ReadLine();
            if (!int.TryParse(s, out mark))
            {
                Console.WriteLine("Invalid input data!");
                return;
            }
            
            List<Student> findResults = students.FindAll(mark);
            findResults.Sort();
            if (findResults.Count == 0)
                Console.WriteLine("Null");
            else
                foreach (var student in findResults)
                    Console.WriteLine(student);

        }
    }
}

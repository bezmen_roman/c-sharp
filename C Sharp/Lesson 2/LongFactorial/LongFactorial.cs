﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LongFactorial
{
    class LongFactorial
    {
        private List<int> _nums;

        public LongFactorial(int n)
        {
            _nums = new List<int>();
            _nums.Add(1);

            for (int i = 2; i <= n; ++i)
            {
                Multiplication(i);
            }
        }

        private void Multiplication(int value)
        {
            for (int i = 0; i < _nums.Count; i++)
            {
                _nums[i] = _nums[i] * value;
            }

            for (int i = 0; i < _nums.Count; i++)
            {
                int n = _nums[i] / 10;
                _nums[i] = _nums[i] % 10;
                if (n > 0)
                {
                    if (_nums.Count == i + 1)
                        _nums.Add(n);
                    else
                        _nums[i + 1] += n;
                }
            }
        }

        public override string ToString()
        {
            StringBuilder str = new StringBuilder();
            for (int i = _nums.Count - 1; i >= 0; --i)
            {
                str.Append(_nums[i]);
            }
            return str.ToString();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StudentSort
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Student> students = new List<Student>();

            students.Add(new Student("Vasya", "Pupkun", "Q-1", 21));
            students.Add(new Student("Petya", "Pupkun", "Q-2", 20));
            students.Add(new Student("Vasya", "Ivanov", "Q-1", 19));
            students.Add(new Student("John", "Pupkun", "Q-2", 23));
            students.Add(new Student("Vasya", "Ykin", "Q-1", 20));

            students.Sort();

            foreach (var item in students)
            {
                Console.WriteLine(item);
            }
        }
    }
}

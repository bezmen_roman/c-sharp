﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StudentSort
{
    struct Student : IComparable<Student>
    {
        string _userName;
        string _lastName;
        string _group;
        int _age;

        public Student(string userName, string lastName, string group, int age)
        {
            _userName = userName;
            _lastName = lastName;
            _group = group;
            _age = age;
        }

        public override string ToString()
        {
            string str = "First name: " + _userName + "\n";
            str += "Second name: " + _lastName + "\n";
            str += "Group: " + _group + "\n";
            str += "Age: " + _age + "\n";
            return str;
        }

        public int CompareTo(Student other)
        {
            return (this._userName).CompareTo(other._userName);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IsolatedStorage
{
    class Program
    {
        static void Main(string[] args)
        {
            IsolatedStorageService service = new IsolatedStorageService(@"data.txt");
            service.WriteData("Hello World!!!\nIsolated Storage!");
            service.ReadAll();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.IO.IsolatedStorage;

namespace IsolatedStorage
{
    class IsolatedStorageService
    {
        public string dir { get; private set; }
        public IsolatedStorageService(string _dir){
            dir = _dir;
        }
        public void WriteData(string data)
        {
            using (IsolatedStorageFile f = IsolatedStorageFile.GetMachineStoreForDomain())
            {
                using (var s = new IsolatedStorageFileStream(dir, FileMode.OpenOrCreate, f))
                {
                    using (var writer = new StreamWriter(s))
                    {
                        writer.Write(data);
                    }
                }
            }
        }
        public void ReadAll()
        {
            using (IsolatedStorageFile f = IsolatedStorageFile.GetMachineStoreForDomain())
            {
                using (var s = new IsolatedStorageFileStream(dir, FileMode.OpenOrCreate, f))
                {
                    using (var reader = new StreamReader(s))
                    {
                        Console.WriteLine(reader.ReadToEnd());
                    }
                }
            }
        }
    }
}

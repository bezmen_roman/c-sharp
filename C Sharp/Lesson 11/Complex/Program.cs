﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Complex
{
    class Program
    {
        static void Main(string[] args)
        {
            Complex a = new Complex(10,10);
            Complex b = 5.5;
            Complex c = new Complex(2,-8);
            Complex d = new Complex(5.5, 0);
            Console.WriteLine("{0} {1} {2}",a,b,c);
            Console.WriteLine(a == b);
            Console.WriteLine(a - b);
            Console.WriteLine(a + b);
            Console.WriteLine(a * c);
            Console.WriteLine(a / c);
            Console.WriteLine(a.GetHashCode());
            Console.WriteLine(b.Equals(d));
        }
    }
}

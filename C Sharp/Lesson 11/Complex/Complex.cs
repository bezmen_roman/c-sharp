﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Complex
{
    struct Complex
    {
        public double Real { get; private set; }
        public double Imaginary { get; private set; }
        public Complex(double re,double im): this(){
            Real = re;
            Imaginary = im;
        }
        public static Complex operator +(Complex a, Complex b)
        {
            return new Complex(a.Real + b.Real, a.Imaginary + b.Imaginary);
        }
        public static Complex operator -(Complex a, Complex b)
        {
            return new Complex(a.Real - b.Real, a.Imaginary - b.Imaginary);
        }
        public static Complex operator *(Complex a, Complex b)
        {
            return new Complex(a.Real * b.Real - a.Imaginary * b.Imaginary, a.Imaginary * b.Real + a.Real * b.Imaginary);
        }
        public static Complex operator /(Complex a, Complex b)
        {
            double re = (a.Real*b.Real+a.Imaginary*b.Imaginary)/(b.Real*b.Real+b.Imaginary*b.Imaginary);
            double im = (a.Imaginary*b.Real-a.Real*b.Imaginary)/(b.Real*b.Real+b.Imaginary*b.Imaginary);
            return new Complex(re,im);
        }
        public static bool operator ==(Complex a, Complex b)
        {
            return (a.Real==b.Real) && (a.Imaginary==b.Imaginary);
        }
        public static bool operator !=(Complex a, Complex b)
        {
            return (a.Real != b.Real) && (a.Imaginary != b.Imaginary);
        }
        public static implicit operator Complex(double value)
        {
            return new Complex(value, 0);
        }
        public override bool Equals(object obj)
        {
            if (obj == null || GetType() != obj.GetType())
                return false;
            Complex p = (Complex)obj;
            return (Real == p.Real) && (Imaginary == p.Imaginary);
        }
        public override int GetHashCode()
        {
            return this.ToString().GetHashCode();
        } 
        public override string ToString()
        {
            char c = Imaginary < 0 ? '-' : '+';
            return string.Format("{0}{1}{2}i", Real, c, Math.Abs(Imaginary));
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExtMethod
{
    interface IMyCompare<T>
    {
        int Compare(T obj1, T obj2);
    }
}

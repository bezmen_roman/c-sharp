﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExtMethod
{
    class MyCompareInt : IMyCompare<int>
    {
        public int Compare(int obj1, int obj2)
        {
            return obj1 - obj2;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExtMethod
{
    static class ExtensionEnumerable
    {
        public static IEnumerable<T> MySort<T>(this IEnumerable<T> enumerable, IMyCompare<T> compare)
        {
            var list = new List<T>();
            foreach (var item in enumerable)
            {
                list.Add(item);
            }

            for (int i = 0; i < list.Count - 1; ++i)
            {
                for (int j = 0; j < list.Count - 1 - i; ++j)
                {
                    if (compare.Compare(list[j], list[j + 1]) > 0)
                    {
                        var t = list[j];
                        list[j] = list[j + 1];
                        list[j + 1] = t;
                    }
                }
            }

            return (IEnumerable<T>)list;
        }
        public static IEnumerable<T> MySort<T>(this IEnumerable<T> enumerable, Func<T, T, int> compare)
        {
            var list = new List<T>();
            foreach (var item in enumerable)
            {
                list.Add(item);
            }

            for (int i = 0; i < list.Count - 1; ++i)
            {
                for (int j = 0; j < list.Count - 1 - i; ++j)
                {
                    if (compare(list[j], list[j + 1]) > 0)
                    {
                        var t = list[j];
                        list[j] = list[j + 1];
                        list[j + 1] = t;
                    }
                }
            }
            return (IEnumerable<T>)list;
        }
    }
}

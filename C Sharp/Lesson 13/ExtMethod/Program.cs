﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExtMethod
{
    class Program
    {
        static void Main(string[] args)
        {
            List<int> list1 = new List<int>() { -1, 4, 6, 5, 9, -10, 1, 3 };
            List<int> list2 = new List<int>() { 1, 3, -1, 5, 9, -1, 0, 3 };

            Console.WriteLine("List 1:");
            foreach (var item in list1)
            {
                Console.Write("{0}\t", item);
            }
            Console.WriteLine();

            Console.WriteLine("List 2:");
            foreach (var item in list2)
            {
                Console.Write("{0}\t", item);
            }
            Console.WriteLine();

            MyCompareInt compare = new MyCompareInt();
            var sortList1 = list1.MySort(compare);
            var sortList2 = list2.MySort((a, b) => (a - b));

            Console.WriteLine("Sort list 1:");
            foreach (var item in sortList1)
            {
                Console.Write("{0}\t", item);
            }
            Console.WriteLine();

            Console.WriteLine("Sort list 2:");
            foreach (var item in sortList2)
            {
                Console.Write("{0}\t", item);
            }
            Console.WriteLine();
        }
    }
}
